/*
Enter your query he
Write a query to print all prime numbers less than or equal to 1000 . Print your result on a single line, and use the ampersand (&) character as your separator (instead of a space).

For example, the output for all prime numbers <=10  would be:
*/
SET @max := 1000;

WITH RECURSIVE
Numbers AS (
    SELECT 2 as n
    UNION
    SELECT n+1 as n FROM Numbers WHERE n < @max
),
Primes AS (
    SELECT 2 as p
    UNION
    SELECT O.n AS p FROM Numbers AS O WHERE 
      ( SELECT MIN(O.n % I.n) FROM Numbers AS I
    WHERE I.n <= CEIL(POW(O.n,0.5)) )
)

SELECT GROUP_CONCAT(p SEPARATOR '&') FROM Primes;